package dao;

import dao.remote.LoanDAORemote;
import dto.LoanDTO;
import dto.UserDTO;
import mapping.DtoToEntity;
import mapping.EntityToDTO;
import model.Loan;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class LoansDAO implements LoanDAORemote {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<LoanDTO> getLoansForUser(UserDTO user) {
        List<Loan> loans = entityManager.createNamedQuery("findLoansByUsername", Loan.class)
                .setParameter("username", user.getUsername())
                .getResultList();

        return loans.stream()
                    .map(EntityToDTO::get)
                    .collect(Collectors.toList());
    }

    @Override
    public LoanDTO findById(int id) {
        Loan loan = entityManager.find(Loan.class, id);
        return EntityToDTO.get(loan);
    }

    @Override
    public List<LoanDTO> findAll() {
        return null;
    }

    @Override
    public Integer create(LoanDTO entity) {
        Loan loan = DtoToEntity.get(entity);
        entityManager.persist(loan);
        entityManager.flush();
        return loan.getIdLoan();
    }

    @Override
    public LoanDTO update(LoanDTO loanDTO) {
        Loan loan = DtoToEntity.get(loanDTO);
        loan.setIdLoan(loanDTO.getId());
        loan = entityManager.merge(loan);
        return EntityToDTO.get(loan);
    }

    @Override
    public void delete(int id) {

    }
}
