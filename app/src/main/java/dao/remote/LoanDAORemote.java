package dao.remote;

import dao.GenericDAO;
import dto.LoanDTO;
import dto.UserDTO;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface LoanDAORemote  extends GenericDAO<LoanDTO> {
    List<LoanDTO> getLoansForUser(UserDTO user);
}
