package dao.remote;

import dao.GenericDAO;
import dto.ItemDTO;
import dto.LoanDTO;
import dto.UserDTO;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface ItemDAORemote extends GenericDAO<ItemDTO> {
    List<ItemDTO> getItemsForLoan(LoanDTO loan);
}
