package dao.remote;

import dao.GenericDAO;
import dto.LoginDTO;
import dto.UserDTO;

import javax.ejb.Remote;
import java.util.Optional;

@Remote
public interface UserDAORemote extends GenericDAO<UserDTO> {
    Optional<UserDTO> loginUser(LoginDTO loginDTO);
}
