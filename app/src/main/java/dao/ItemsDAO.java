package dao;

import dao.remote.ItemDAORemote;
import dto.ItemDTO;
import dto.LoanDTO;
import mapping.DtoToEntity;
import mapping.EntityToDTO;
import model.Item;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class ItemsDAO  implements ItemDAORemote {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ItemDTO findById(int id) {
        return null;
    }

    @Override
    public List<ItemDTO> findAll() {
        return null;
    }

    @Override
    public Integer create(ItemDTO entity) {
        Item item = DtoToEntity.get(entity);
        entityManager.persist(item);
        entityManager.flush();
        return item.getIdItem();
    }

    @Override
    public List<ItemDTO> getItemsForLoan(LoanDTO loan) {
        List<Item> items = entityManager.createNamedQuery("findItemsByLoanId", Item.class)
                .setParameter("loanid", loan.getId())
                .getResultList();
        return items.stream()
                .map(EntityToDTO::get)
                .collect(Collectors.toList());
    }

    @Override
    public ItemDTO update(ItemDTO entity) {
        return null;
    }

    @Override
    public void delete(int id) {

    }

}
