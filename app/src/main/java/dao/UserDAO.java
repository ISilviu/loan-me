package dao;

import dao.remote.UserDAORemote;
import dto.LoginDTO;
import dto.UserDTO;
import mapping.DtoToEntity;
import mapping.EntityToDTO;
import model.User;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class UserDAO implements UserDAORemote {
    static final Logger LOGGER = Logger.getLogger(UserDAO.class.getName());

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public UserDTO findById(int id) {
        User user = entityManager.find(User.class, id);
        return EntityToDTO.get(user);
    }

    @Override
    public List<UserDTO> findAll() {
        Query query = entityManager.createQuery("SELECT u FROM User u");
        @SuppressWarnings("unchecked")
        List<User> users = query.getResultList();
        System.out.println(users.toString());
        List<UserDTO> dtoUsers = new ArrayList<>();
        for (User user : users) {
            dtoUsers.add(EntityToDTO.get(user));
        }
        return dtoUsers;
    }

    @Override
    public Integer create(UserDTO userDTO) {
        User user = DtoToEntity.get(userDTO);
        entityManager.persist(user);
        entityManager.flush();
        return user.getIdUser();
    }

    @Override
    public UserDTO update(UserDTO userDTO) {
        User user = DtoToEntity.get(userDTO);
        user.setIdUser(userDTO.getId());
        user = entityManager.merge(user);
        return userDTO;
    }

    @Override
    public void delete(int id) {
        User user = entityManager.find(User.class, id);
        entityManager.remove(user);

    }

    @Override
    public Optional<UserDTO> loginUser(LoginDTO loginDTO) {
        User user = null;
        try {
            user = entityManager.createNamedQuery("findUserByUsername", User.class)
                    .setParameter("username", loginDTO.getUsername()).getSingleResult();
        } catch (NoResultException e) {
            return Optional.empty();
        }
        if (!loginDTO.getPassword().equals(user.getPassword())) {
            return Optional.empty();
        }
        return Optional.of(EntityToDTO.get(user));
    }
}
