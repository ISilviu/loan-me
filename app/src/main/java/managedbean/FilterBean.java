package managedbean;

import dto.FilterDTO;
import dto.LoanDTO;
import dto.UserDTO;
import service.remote.FilterService;
import service.remote.LoanService;
import service.remote.MessagingService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Named
@SessionScoped
public class FilterBean implements Serializable {

    @EJB
    LoanService loanService;

    @EJB
    FilterService filterService;

    @EJB
    MessagingService messagingService;

    private final String LOANS_TOPIC_NAME = "Loans";

    List<LoanDTO> loans;
    FilterDTO filterDTO;
    UserDTO userDTO;
    List<FilterDTO.Timing> timings;
    boolean wasFiltered;
    boolean wasCleared;

    @PostConstruct
    private void initialize() {
        wasCleared = false;
        wasFiltered = false;
        userDTO = (UserDTO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userDTO");
        filterDTO = new FilterDTO();
        timings = Arrays.asList(FilterDTO.Timing.values());
    }

    public String markAsReceived(int loanId) {
        LoanDTO loanDTO = loanService.markAsReceived(loanId);

        messagingService.publishMessage(messagingService.getTopicUrl(LOANS_TOPIC_NAME), loanDTO.getBorrowerName());

        if(loans != null) {
            Optional<LoanDTO> optionalLoanDTO = loans.stream().filter(l -> l.getId() == loanId).findFirst();
            if(optionalLoanDTO.isPresent()) {
                LoanDTO uiLoanDto = optionalLoanDTO.get();
                uiLoanDto.setReturnedDate(loanDTO.getReturnedDate());
                uiLoanDto.setIsReturned(loanDTO.getIsReturned());
            }
        }
        return "dashboard?faces-redirect=true";
    }

    public String reset() {
        wasCleared = true;
        loans = null;
        return "dashboard?faces-redirect=true";
    }

    public String filter() {
        wasCleared = false;
        wasFiltered = true;
        loans = filterService.filterLoans(loanService.getLoansForUser(userDTO), filterDTO);
        return "dashboard?faces-redirect=true";
    }

    public List<LoanDTO> getLoans() {
        return loans;
    }

    public void setLoans(List<LoanDTO> loans) {
        this.loans = loans;
    }

    public boolean isWasCleared() {
        return wasCleared;
    }

    public void setWasCleared(boolean wasCleared) {
        this.wasCleared = wasCleared;
    }

    public FilterDTO getFilterDTO() {
        return filterDTO;
    }

    public void setFilterDTO(FilterDTO filterDTO) {
        this.filterDTO = filterDTO;
    }

    public List<FilterDTO.Timing> getTimings() {
        return timings;
    }

    public void setTimings(List<FilterDTO.Timing> timings) {
        this.timings = timings;
    }

    public boolean isWasFiltered() {
        return wasFiltered;
    }

    public void setWasFiltered(boolean wasFiltered) {
        this.wasFiltered = wasFiltered;
    }
}
