package managedbean;

import dto.ItemDTO;
import dto.LoanDTO;
import dto.UserDTO;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;
import service.remote.FileUploadService;
import service.remote.LoanService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Named
@ViewScoped
public class NewLoanBean implements Serializable {

    @EJB
    LoanService loanService;

    @EJB
    FileUploadService fileUploadService;


    ItemDTO itemDTO;
    LoanDTO loanDTO;

    Date loanDate;
    String borrowerName;
    boolean isUploading;

    @PostConstruct
    private void initialize() {
        isUploading = false;
        itemDTO = new ItemDTO();
        loanDTO = new LoanDTO();
    }

    public void upload(FileUploadEvent event) {
        isUploading = true;
        UserDTO userDTO = (UserDTO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userDTO");

        final UploadedFile uploadedFile = event.getFile();
        final String fileName = uploadedFile.getFileName();
        final String imageKey = String.format("%s/%s", userDTO.getUsername(), fileName);

        Optional<String> url = fileUploadService.uploadFile(uploadedFile.getContent(), imageKey, uploadedFile.getSize());
        if (url.isPresent()) {
            itemDTO.setImageUrl(url.get());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(String.format("%s was uploaded.", fileName)));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, String.format("%s couldn't be uploaded.", fileName), null));
        }
        isUploading = false;
    }

    public String addLoan(){
        if(isUploading) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "The image is still uploading. Please wait.", null));
            return null;
        }
        UserDTO userDTO = (UserDTO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userDTO");
        loanDTO = new LoanDTO(loanDate, false, userDTO.getId(), Collections.EMPTY_LIST, borrowerName);
        List<ItemDTO> itemsDTOs = Collections.singletonList(itemDTO);
        System.out.println(itemDTO);
        loanService.addLoan(loanDTO, itemsDTOs);
        return "dashboard";
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public Date getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(Date loanDate) {
        this.loanDate = loanDate;
    }

    public LoanDTO getLoanDTO() {
        return loanDTO;
    }

    public void setLoanDTO(LoanDTO loanDTO) {
        this.loanDTO = loanDTO;
    }

    public ItemDTO getItemDTO() {
        return itemDTO;
    }

    public void setItemDTO(ItemDTO itemDTO) {
        this.itemDTO = itemDTO;
    }
}
