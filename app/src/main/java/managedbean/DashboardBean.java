package managedbean;

import dto.LoanDTO;
import dto.UserDTO;
import service.remote.LoanService;
import service.remote.UserMessageService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class DashboardBean  implements Serializable {

    @EJB
    LoanService loanService;

    @EJB
    UserMessageService userMessageService;

    UserDTO userDTO;
    List<LoanDTO> loans;

    @PostConstruct
    private void initialize(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        userDTO = (UserDTO) facesContext.getExternalContext().getSessionMap().get("userDTO");
        loans = loanService.getLoansForUser(userDTO);
    }

    public void showMessage(){
        FacesContext.getCurrentInstance().addMessage(null, userMessageService.composeDashboardMessage(loans));
    }

    public String addLoan(){
        return "new_loan?faces-redirect=true";
    }

    public List<LoanDTO> getLoans() { return loans; }

    public void setLoans(List<LoanDTO> loans) {
        this.loans = loans;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
