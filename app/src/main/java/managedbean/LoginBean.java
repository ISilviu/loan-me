package managedbean;

import dto.LoginDTO;
import dto.UserDTO;
import service.remote.SimultaneousAccessVerifierService;
import service.remote.UserMessageService;
import service.remote.UserService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Optional;

@Named
@RequestScoped
public class LoginBean implements Serializable {

    @EJB
    UserService userService;

    @EJB
    SimultaneousAccessVerifierService accessVerifier;

    UserDTO userDTO;
    LoginDTO loginDTO;

    @PostConstruct
    private void initialize() {
        loginDTO = new LoginDTO();
    }

    public String loginUser() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Optional<UserDTO> optionalUserDTO = userService.loginUser(loginDTO);
        if(optionalUserDTO.isPresent()) {
            userDTO = optionalUserDTO.get();
            if(accessVerifier.isAlreadyLoggedIn(userDTO)) {
                String ipAddress = accessVerifier.getIpAddress(userDTO).get();
                String message = ipAddress.equals("0:0:0:0:0:0:0:1") ?
                        "Someone else from your local network is already logged in on this account." :
                        String.format("Someone else having the IP %s is already logged in on this account", ipAddress);
                facesContext.addMessage("loginForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
                return null;
            }
            accessVerifier.mark(SimultaneousAccessVerifierService.State.LOGGED_IN, userDTO);
            facesContext.getExternalContext().getSessionMap().put("userDTO", userDTO);
            return "dashboard?faces-redirect=true";
        } else {
            facesContext.addMessage("loginForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, "The credentials did not match.", null));
            return null;
        }
    }

    public String logout() {
        UserDTO userDTO = (UserDTO) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userDTO");
        accessVerifier.mark(SimultaneousAccessVerifierService.State.LOGGED_OUT, userDTO);
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/index?faces-redirect=true";
    }

    public LoginDTO getLoginDTO() {
        return loginDTO;
    }

    public void setLoginDTO(LoginDTO loginDTO) {
        this.loginDTO = loginDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
