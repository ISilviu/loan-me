package model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "loan")
@NamedQuery(name = "findLoansByUsername", query = "SELECT l FROM Loan l WHERE l.userByUserId.username = :username")
public class Loan implements Serializable  {
    private static final long serialVersionUID = 1L;
    private Integer idLoan;
    private Date dateLoaned;
    private Boolean isReturned;
    private Date returnedDate;
    private Integer userId;
    private String borrowerName;
    private Collection<Item> itemsByIdLoan;
    private User userByUserId;

    public Loan(){}

    public Loan(int id, Date dateLoaned, Boolean isReturned, Date returnedDate, List<Item> items, String borrowerName) {
        this.idLoan = id;
        this.dateLoaned = dateLoaned;
        this.isReturned = isReturned;
        this.returnedDate = returnedDate;
        this.itemsByIdLoan = items;
        this.borrowerName = borrowerName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_loan")
    public Integer getIdLoan() {
        return idLoan;
    }

    public void setIdLoan(Integer idLoan) {
        this.idLoan = idLoan;
    }

    @Basic
    @Column(name = "date_loaned")
    public Date getDateLoaned() {
        return dateLoaned;
    }

    public void setDateLoaned(Date dateLoaned) {
        this.dateLoaned = dateLoaned;
    }

    @Basic
    @Column(name = "is_returned")
    public Boolean getIsReturned() {
        return isReturned;
    }

    public void setIsReturned(Boolean isReturned) {
        this.isReturned = isReturned;
    }

    @Basic
    @Column(name = "returned_date")
    public Date getReturnedDate() {
        return returnedDate;
    }

    public void setReturnedDate(Date returnedDate) {
        this.returnedDate = returnedDate;
    }

    @Basic
    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "borrower_name")
    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return Objects.equals(idLoan, loan.idLoan) &&
                Objects.equals(dateLoaned, loan.dateLoaned) &&
                Objects.equals(isReturned, loan.isReturned) &&
                Objects.equals(returnedDate, loan.returnedDate) &&
                Objects.equals(userId, loan.userId) &&
                Objects.equals(borrowerName, loan.borrowerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idLoan, dateLoaned, isReturned, returnedDate, userId, borrowerName);
    }

    @OneToMany(mappedBy = "loanByLoanId")
    public Collection<Item> getItemsByIdLoan() {
        return itemsByIdLoan;
    }

    public void setItemsByIdLoan(Collection<Item> itemsByIdLoan) {
        this.itemsByIdLoan = itemsByIdLoan;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id_user", nullable = false, insertable = false, updatable = false)
    public User getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(User userByUserId) {
        this.userByUserId = userByUserId;
    }
}
