package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "item")
@NamedQuery(name = "findItemsByLoanId", query = "SELECT i FROM Item i WHERE i.loanId = :loanid")
public class Item implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer idItem;
    private String name;
    private Integer quantity;
    private String imageUrl;
    private Integer loanId;
    private Loan loanByLoanId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_item")
    public Integer getIdItem() {
        return idItem;
    }

    public void setIdItem(Integer idItem) {
        this.idItem = idItem;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "quantity")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "loan_id")
    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(idItem, item.idItem) &&
                Objects.equals(name, item.name) &&
                Objects.equals(quantity, item.quantity) &&
                Objects.equals(imageUrl, item.imageUrl) &&
                Objects.equals(loanId, item.loanId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idItem, name, quantity, imageUrl, loanId);
    }

    @ManyToOne
    @JoinColumn(name = "loan_id", referencedColumnName = "id_loan", insertable = false, updatable = false)
    public Loan getLoanByLoanId() {
        return loanByLoanId;
    }

    public void setLoanByLoanId(Loan loanByLoanId) {
        this.loanByLoanId = loanByLoanId;
    }
}
