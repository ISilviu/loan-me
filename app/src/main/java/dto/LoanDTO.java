package dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.List;

public class LoanDTO implements Serializable {

    private int id;
    private Date dateLoaned;
    private Boolean isReturned;
    private Date returnedDate;
    private Collection<ItemDTO> items;
    private int userId;
    private String state;
    private String borrowerName;

    public LoanDTO(Date dateLoaned, byte isReturned, Date returnedDate, List<ItemDTO> items, String borrowerName) {

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateLoaned() {
        return dateLoaned;
    }

    public void setDateLoaned(Date dateLoaned) {
        this.dateLoaned = dateLoaned;
    }

    public Boolean getIsReturned() {
        return isReturned;
    }

    public void setIsReturned(Boolean isReturned) {
        this.isReturned = isReturned;
        initializeState();
    }

    public Date getReturnedDate() {
        return returnedDate;
    }

    public void setReturnedDate(Date returnedDate) {
        this.returnedDate = returnedDate;
    }

    public Collection<ItemDTO> getItems() {
        return items;
    }

    public void setItems(Collection<ItemDTO> items) {
        this.items = items;
    }

    private void initializeState(){
        state = this.isReturned ? "Received" : "Not received yet";
    }

    public LoanDTO(){}

    public LoanDTO(Date dateLoaned, Boolean isReturned, Date returnedDate, Collection<ItemDTO> items, String borrowerName) {
        this.dateLoaned = dateLoaned;
        this.isReturned = isReturned;
        this.returnedDate = returnedDate;
        this.items = items;
        this.borrowerName = borrowerName;
        initializeState();
    }

    public LoanDTO(java.util.Date dateLoaned, Boolean isReturned,  java.util.Date returnedDate, Collection<ItemDTO> items, String borrowerName) {
        this.dateLoaned = new Date(dateLoaned.getTime());
        this.isReturned = isReturned;
        this.returnedDate = new Date(returnedDate.getTime());
        this.items = items;
        this.borrowerName = borrowerName;
        initializeState();
    }

    public LoanDTO(java.util.Date dateLoaned, Boolean isReturned, int userId, Collection<ItemDTO> items, String borrowerName) {
        this.dateLoaned = new Date(dateLoaned.getTime());
        this.isReturned = isReturned;
        this.returnedDate = null;
        this.items = items;
        this.userId = userId;
        this.borrowerName = borrowerName;
        initializeState();
    }

    @Override
    public String toString() {
        return "LoanDTO{" +
                "id=" + id +
                ", dateLoaned=" + dateLoaned +
                ", isReturned=" + isReturned +
                ", returnedDate=" + returnedDate +
                ", items=" + items +
                ", userId=" + userId +
                ", state='" + state + '\'' +
                ", borrowerName='" + borrowerName + '\'' +
                '}';
    }
}
