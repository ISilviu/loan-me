package dto;

import model.Loan;

import java.io.Serializable;
import java.util.Collection;

public class UserDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private String username;
    private String password;
    private Collection<LoanDTO> loansByIdUser;
    private String firstName;
    private String lastName;

    public Collection<LoanDTO> getLoansByIdUser() {
        return loansByIdUser;
    }

    public void setLoansByIdUser(Collection<LoanDTO> loansByIdUser) {
        this.loansByIdUser = loansByIdUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserDTO() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDTO(String username, String password, Collection<LoanDTO> loans, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.loansByIdUser = loans;
    }

    @Override
    public String toString() {
        return "UserDTO [id=" + id + ", username=" + username + ", password=" + password + "]";
    }

}