package filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/index.xhtml"})
public class SessionFilter implements Filter {

    public static final String DASHBOARD_PAGE = "/dashboard.xhtml";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        HttpSession session = httpServletRequest.getSession(false);

        boolean isLoggedIn = session != null && session.getAttribute("userDTO") != null;
        if(isLoggedIn) {
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + DASHBOARD_PAGE);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}
