package service;

import auth.InCodeCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import service.remote.FileUploadService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Optional;

@Stateless
@LocalBean
public class S3FileUploadService implements FileUploadService {

    private final AmazonS3Client client;

    public S3FileUploadService() {
        BasicSessionCredentials basicSessionCredentials = new BasicSessionCredentials(
                InCodeCredentialsProvider.AWS_ACCESS_KEY_ID,
                InCodeCredentialsProvider.AWS_SECRET_ACCESS_KEY,
                InCodeCredentialsProvider.AWS_SESSION_TOKEN
        );

        client = (AmazonS3Client) AmazonS3ClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(basicSessionCredentials))
                .withPathStyleAccessEnabled(true)
                .build();
    }

    @Override
    public Optional<String> uploadFile(byte[] inputBytes, String uploadName, long fileSize) {
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(fileSize);
            InputStream inputStream = new ByteArrayInputStream(inputBytes);
            PutObjectRequest putObjectRequest = new PutObjectRequest("loan-me", uploadName, inputStream, objectMetadata).withCannedAcl(CannedAccessControlList.PublicRead);
            try {
                client.putObject(putObjectRequest);
                return Optional.of(client.getUrl("loan-me", uploadName).toString());
            } catch (Exception e) {
                e.printStackTrace();
                return Optional.empty();
            }
    }
}
