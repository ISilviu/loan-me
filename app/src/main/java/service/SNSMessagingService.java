package service;

import auth.InCodeCredentialsProvider;
import service.remote.MessagingService;
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class SNSMessagingService implements MessagingService {

    private static final String accountNumber = "677232553113";

    private final SnsClient client;
    private static final String AWS_ACCOUNT_NUMBER = "AWS_ACCOUNT_NUMBER";

    public SNSMessagingService() {
        AwsSessionCredentials sessionCredentials = AwsSessionCredentials.create(
                InCodeCredentialsProvider.AWS_ACCESS_KEY_ID,
                InCodeCredentialsProvider.AWS_SECRET_ACCESS_KEY,
                InCodeCredentialsProvider.AWS_SESSION_TOKEN
        );
        client = SnsClient.builder()
                .credentialsProvider(StaticCredentialsProvider.create(sessionCredentials))
                .region(Region.US_EAST_1)
                .build();
    }

    @Override
    public List<String> listTopics() {
        try {
            ListTopicsRequest request = ListTopicsRequest.builder()
                    .build();
            ListTopicsResponse result = client.listTopics(request);
            System.out.println(result.topics());
            return result.topics().stream().map(Topic::topicArn).collect(Collectors.toList());
        } catch (SnsException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @Override
    public boolean hasTopic(String topicName) {
        List<String> topics = listTopics();
        if(topics.isEmpty()) {
            return false;
        }
        return topics.stream().anyMatch(topic -> topic.contains(topicName));
    }

    @Override
    public Optional<String> createTopic(String topicName) {
        try {
           CreateTopicRequest request = CreateTopicRequest.builder()
                   .name(topicName)
                   .build();
           CreateTopicResponse result = client.createTopic(request);
            System.out.println("ARN Name: " + result.topicArn());
           return Optional.of(result.topicArn());
        } catch (SnsException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public String getTopicUrl(String topicName) {
        System.out.println("Account number: "  + accountNumber);
        return String.format("arn:aws:sns:us-east-1:%s:%s", accountNumber, topicName);
    }

    @Override
    public void publishMessage(String topicName, String message) {
        try {

            PublishRequest request = PublishRequest.builder()
                    .message(message)
                    .topicArn(topicName)
                    .build();
            PublishResponse result = client.publish(request);
            System.out.println("Published: " + message + " " + result.messageId() + " Message sent. Status was " + result.sdkHttpResponse().statusCode());
        } catch (SnsException e){
            e.printStackTrace();
        }
    }
}
