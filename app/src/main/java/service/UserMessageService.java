package service;

import dto.LoanDTO;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import java.util.Collection;

@Stateless
@LocalBean
public class UserMessageService implements service.remote.UserMessageService {

    private final String LOCALHOST_IP_ADDRESS = "0:0:0:0:0:0:0:1";

    @Override
    public FacesMessage composeDashboardMessage(Collection<LoanDTO> loans) {
        if(loans.isEmpty()) {
            return new FacesMessage(FacesMessage.SEVERITY_INFO, "No loans", "You don't have any loans for now.");
        }

        long totalLoans = loans.size();
        long loansReturned = loans.stream().filter(LoanDTO::getIsReturned).count();
        long returnedPercent = loansReturned * 100 / totalLoans;

        final int LOW_RETURN_RATE = 30;
        final int HIGH_RETURN_RATE = 70;
        final String DETAIL_MESSAGE = String.format("%s%% of your borrowers have returned their loan.", returnedPercent);

        if(returnedPercent >= HIGH_RETURN_RATE) {
           return new FacesMessage(FacesMessage.SEVERITY_INFO,"Good news", DETAIL_MESSAGE);
        } else if(returnedPercent >= LOW_RETURN_RATE) {
            return new FacesMessage(FacesMessage.SEVERITY_WARN,"Good news", DETAIL_MESSAGE);
        } else {
            return new FacesMessage(FacesMessage.SEVERITY_FATAL,"Bad news", String.format("%s%% of your borrowers have returned their loan. Be more careful with your loans.", returnedPercent));
        }
    }

    @Override
    public FacesMessage composeSimultaneousAccessMessage(String ipAddress) {
        String message = ipAddress.equals(LOCALHOST_IP_ADDRESS) ?
                "Someone else from your local network is already logged in on this account." :
                String.format("Someone else having the IP %s is already logged in on this account", ipAddress);
        return new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null);
    }
}
