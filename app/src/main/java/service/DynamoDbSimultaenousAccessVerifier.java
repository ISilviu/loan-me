package service;

import auth.InCodeCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AmazonDynamoDBException;
import dto.UserDTO;
import service.remote.SimultaneousAccessVerifierService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@LocalBean
@Stateless
public class DynamoDbSimultaenousAccessVerifier implements SimultaneousAccessVerifierService {

    private final Table usersTable;

    private final SimpleDateFormat format;

    private final String KEY_NAME = "username";
    private final String IP_ADDRESS_NAME = "ip_address";
    private final String DATE_LOGGED_NAME = "date_logged";

    private final long SESSION_TIME_MINUTES = 5;

    private String ipAddress;

    public DynamoDbSimultaenousAccessVerifier() {
        format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        BasicSessionCredentials basicSessionCredentials = new BasicSessionCredentials(
                InCodeCredentialsProvider.AWS_ACCESS_KEY_ID,
                InCodeCredentialsProvider.AWS_SECRET_ACCESS_KEY,
                InCodeCredentialsProvider.AWS_SESSION_TOKEN
        );

        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(basicSessionCredentials))
                .build();
        DynamoDB database = new DynamoDB(client);
        usersTable = database.getTable("logged_users");
    }

    @Override
    public boolean isAlreadyLoggedIn(UserDTO userDTO) {
        Item item = null;
        try {
            item = usersTable.getItem(KEY_NAME, userDTO.getUsername());
        }
        catch (AmazonDynamoDBException e) {
            return false;
        }

        if(item != null) {
            ipAddress = item.getString(IP_ADDRESS_NAME);
            try {
                Date loggedInDate = format.parse(item.getString(DATE_LOGGED_NAME));
                long differenceInMilliseconds = Math.abs(Calendar.getInstance().getTimeInMillis() - loggedInDate.getTime());
                long differenceInMinutes =  TimeUnit.MINUTES.convert(differenceInMilliseconds, TimeUnit.MILLISECONDS);
                return differenceInMinutes <= SESSION_TIME_MINUTES;
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    @Override
    public Optional<String> getIpAddress(UserDTO userDTO) {
        if(ipAddress != null) {
            return Optional.of(ipAddress);
        }
        Item item = null;
        try {
            item = usersTable.getItem(KEY_NAME, userDTO.getUsername());
        } catch (AmazonDynamoDBException e) {
            return Optional.empty();
        }
        return item == null ? Optional.empty() : Optional.of(item.getString(IP_ADDRESS_NAME));
    }

    @Override
    public void mark(State state, UserDTO userDTO) {
        switch (state) {
            case LOGGED_IN:
                HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                Item user = new Item()
                        .withPrimaryKey(KEY_NAME, userDTO.getUsername())
                        .withString(IP_ADDRESS_NAME, request.getLocalAddr())
                        .withString(DATE_LOGGED_NAME, format.format(Calendar.getInstance().getTime()));
                try {
                    if(usersTable.getItem(KEY_NAME, userDTO.getUsername()) != null){
                        usersTable.deleteItem(KEY_NAME, userDTO.getUsername());
                    }
                    usersTable.putItem(user);
                } catch (AmazonDynamoDBException e)  {
                    e.printStackTrace();
                }
                break;
            case LOGGED_OUT:
                try {
                    usersTable.deleteItem(KEY_NAME, userDTO.getUsername());
                } catch (AmazonDynamoDBException ignored) {}
                break;
        }
    }
}
