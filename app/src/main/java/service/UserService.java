package service;

import dao.remote.UserDAORemote;
import dto.LoginDTO;
import dto.UserDTO;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Optional;

@Stateless
@LocalBean
public class UserService implements service.remote.UserService {

    @EJB
    UserDAORemote userDAO;

    @Override
    public Optional<UserDTO> loginUser(LoginDTO loginDTO) {
        return userDAO.loginUser(loginDTO);
    }
}
