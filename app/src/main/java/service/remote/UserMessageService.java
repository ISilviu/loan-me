package service.remote;

import dto.LoanDTO;

import javax.ejb.Remote;
import javax.faces.application.FacesMessage;
import java.util.Collection;

@Remote
public interface UserMessageService {
    FacesMessage composeDashboardMessage(Collection<LoanDTO> loans);
    FacesMessage composeSimultaneousAccessMessage(String ipAddress);
}
