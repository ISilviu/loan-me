package service.remote;

import dto.ItemDTO;
import dto.LoanDTO;
import dto.UserDTO;

import javax.ejb.Remote;
import java.util.Collection;
import java.util.List;

@Remote
public interface LoanService {
    void addLoan(LoanDTO loanDTO, Collection<ItemDTO> itemDTOs);
    List<LoanDTO> getLoansForUser(UserDTO userDTO);
    LoanDTO markAsReceived(int loanId);
}
