package service.remote;

import dto.UserDTO;

import javax.ejb.Remote;
import java.util.Optional;

@Remote
public interface SimultaneousAccessVerifierService {
    enum State {
        LOGGED_IN,
        LOGGED_OUT
    }

    boolean isAlreadyLoggedIn(UserDTO userDTO);
    Optional<String> getIpAddress(UserDTO userDTO);
    void mark(State state, UserDTO userDTO);
}
