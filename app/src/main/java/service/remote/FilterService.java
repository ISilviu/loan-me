package service.remote;

import dto.FilterDTO;
import dto.LoanDTO;

import javax.ejb.Remote;
import java.util.Collection;
import java.util.List;

@Remote
public interface FilterService {
    List<LoanDTO> filterLoans(List<LoanDTO> loans, FilterDTO filterDTO);
}
