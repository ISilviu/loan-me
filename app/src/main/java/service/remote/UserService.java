package service.remote;

import dto.LoginDTO;
import dto.UserDTO;

import javax.ejb.Remote;
import java.util.Optional;

@Remote
public interface UserService {
    Optional<UserDTO> loginUser(LoginDTO loginDTO);
}
