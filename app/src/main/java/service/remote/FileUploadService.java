package service.remote;

import javax.ejb.Remote;
import java.util.Optional;

@Remote
public interface FileUploadService {
    Optional<String> uploadFile(byte[] inputBytes, String uploadName, long fileSize);
}
