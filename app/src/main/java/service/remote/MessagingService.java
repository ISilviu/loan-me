package service.remote;

import javax.ejb.Remote;
import java.util.List;
import java.util.Optional;

@Remote
public interface MessagingService {
    List<String> listTopics();
    boolean hasTopic(String topicName);
    Optional<String> createTopic(String topicName);
    void publishMessage(String topicName, String message);
    String getTopicUrl(String topicName);
}
