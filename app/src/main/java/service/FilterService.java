package service;

import dto.FilterDTO;
import dto.LoanDTO;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class FilterService implements service.remote.FilterService {

    private Predicate<LoanDTO> constructPredicate(FilterDTO filterDTO){
        Predicate<LoanDTO> filterPredicate = null;
        if(filterDTO.getBorrowerName() != null && filterDTO.getLoanDate() != null && filterDTO.getTiming() != null) {
            switch(filterDTO.getTiming()) {
                case Before:
                    filterPredicate = loanDTO ->
                            loanDTO.getBorrowerName().contains(filterDTO.getBorrowerName()) &&
                                    loanDTO.getDateLoaned().before(filterDTO.getLoanDate());
                    break;
                case After:
                    filterPredicate = loanDTO ->
                            loanDTO.getBorrowerName().contains(filterDTO.getBorrowerName()) &&
                                    loanDTO.getDateLoaned().after(filterDTO.getLoanDate());
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + filterDTO.getTiming());
            }
        } else if(filterDTO.getBorrowerName() != null && !filterDTO.getBorrowerName().equals("")) {
            filterPredicate = loanDTO -> loanDTO.getBorrowerName().contains(filterDTO.getBorrowerName());
        } else if(filterDTO.getLoanDate() != null && filterDTO.getTiming() != null) {
            switch(filterDTO.getTiming()) {
                case Before:
                    filterPredicate = loanDTO -> loanDTO.getDateLoaned().before(filterDTO.getLoanDate());
                    break;
                case After:
                    filterPredicate = loanDTO -> loanDTO.getDateLoaned().after(filterDTO.getLoanDate());
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + filterDTO.getTiming());
            }
        }
        return filterPredicate;
    }
    @Override
    public List<LoanDTO> filterLoans(List<LoanDTO> loans, FilterDTO filterDTO) {
       Predicate<LoanDTO> filterPredicate = constructPredicate(filterDTO);
        return filterPredicate == null ? null : loans.stream().filter(filterPredicate).collect(Collectors.toList());
    }
}
