package service;

import dao.remote.ItemDAORemote;
import dao.remote.LoanDAORemote;
import dto.ItemDTO;
import dto.LoanDTO;
import dto.UserDTO;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Stateless
@LocalBean
public class LoanService implements service.remote.LoanService {

    @EJB
    LoanDAORemote loansDAORemote;

    @EJB
    ItemDAORemote itemDAORemote;

    @Override
    public void addLoan(LoanDTO loanDTO, Collection<ItemDTO> itemDTOs) {
        final Integer loanId = loansDAORemote.create(loanDTO);
        itemDTOs.forEach(itemDTO -> {
            itemDTO.setLoanId(loanId);
            itemDAORemote.create(itemDTO);
        });
    }

    @Override
    public List<LoanDTO> getLoansForUser(UserDTO userDTO) {
        List<LoanDTO> loans = loansDAORemote.getLoansForUser(userDTO);
        loans.forEach(loanDTO -> {
            List<ItemDTO> items = itemDAORemote.getItemsForLoan(loanDTO);
            loanDTO.setItems(items);
        });
        return loans;
    }

    @Override
    public LoanDTO markAsReceived(int loanId) {
        final boolean isReturned = true;
        final Date returnedDate = Date.valueOf(LocalDate.now());

        LoanDTO loanDTO = loansDAORemote.findById(loanId);
        loanDTO.setIsReturned(isReturned);
        loanDTO.setReturnedDate(returnedDate);
        loansDAORemote.update(loanDTO);

        return loanDTO;
    }
}
