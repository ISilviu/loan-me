package mapping;

import dto.ItemDTO;
import dto.LoanDTO;
import dto.UserDTO;
import model.Item;
import model.Loan;
import model.User;

import java.util.List;
import java.util.stream.Collectors;

public class EntityToDTO {
    public static UserDTO get(User user){
        List<LoanDTO> loans = user.getLoansByIdUser().stream().map(EntityToDTO::get).collect(Collectors.toList());
        UserDTO userDTO = new UserDTO(user.getUsername(), user.getPassword(), loans, user.getFirstName(), user.getLastName());
        userDTO.setId(user.getIdUser());
        return userDTO;
    }

    public static LoanDTO get(Loan loan){
        List<ItemDTO> items = loan.getItemsByIdLoan().stream().map(EntityToDTO::get).collect(Collectors.toList());
        LoanDTO loanDTO = new LoanDTO(loan.getDateLoaned(), loan.getIsReturned(), loan.getReturnedDate(), items, loan.getBorrowerName());
        loanDTO.setId(loan.getIdLoan());
        loanDTO.setUserId(loan.getUserByUserId().getIdUser());
        return loanDTO;
    }


    public static ItemDTO get(Item item){
        ItemDTO itemDTO = new ItemDTO(item.getName(), item.getQuantity(), item.getImageUrl());
        itemDTO.setIdItem(item.getIdItem());
        return itemDTO;
    }
}
