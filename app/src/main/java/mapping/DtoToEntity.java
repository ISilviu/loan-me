package mapping;

import dto.ItemDTO;
import dto.LoanDTO;
import dto.UserDTO;
import model.Item;
import model.Loan;
import model.User;

import java.util.List;
import java.util.stream.Collectors;

public class DtoToEntity {

    public static User get(UserDTO userDTO) {
        List<Loan> loans = userDTO.getLoansByIdUser().stream().map(DtoToEntity::get).collect(Collectors.toList());
        User user = new User();
        user.setIdUser(userDTO.getId());
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setLoansByIdUser(loans);
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        return user;
    }

    public static Loan get(LoanDTO loanDTO){
        List<Item> items = loanDTO.getItems().stream().map(DtoToEntity::get).collect(Collectors.toList());
        Loan loan = new Loan(loanDTO.getId(), loanDTO.getDateLoaned(), loanDTO.getIsReturned(), loanDTO.getReturnedDate(), items, loanDTO.getBorrowerName());
        loan.setUserByUserId(new User());
        loan.setUserId(loanDTO.getUserId());
        loan.getUserByUserId().setIdUser(loanDTO.getUserId());
        return loan;
    }

    public static Item get(ItemDTO itemDTO) {
        Item item = new Item();
        item.setIdItem(itemDTO.getIdItem());
        item.setName(itemDTO.getName());
        item.setQuantity(itemDTO.getQuantity());
        item.setImageUrl(itemDTO.getImageUrl());
        item.setLoanId(itemDTO.getLoanId());
        return item;
    }
}
