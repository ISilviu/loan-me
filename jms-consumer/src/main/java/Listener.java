import org.json.JSONObject;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class Listener implements MessageListener {

    public void onMessage(Message receivedMessage) {
        try {
            if(receivedMessage != null) {
                String text = ((TextMessage) receivedMessage).getText();
                JSONObject object = new JSONObject(text);
                System.out.println("Received: " +  object.getString("Message"));
                receivedMessage.acknowledge();
            }
        } catch(JMSException e) {
            e.printStackTrace();
        }
    }
}
