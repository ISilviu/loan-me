import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

import javax.jms.*;

public class Main {
    public static void main(String[] args) throws JMSException {
        SQSConnectionFactory connectionFactory = new SQSConnectionFactory(
                new ProviderConfiguration(),
                AmazonSQSClientBuilder.defaultClient()
        );

        SQSConnection connection = connectionFactory.createConnection(DefaultAWSCredentialsProviderChain.getInstance());
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Queue queue = session.createQueue("loan-received");
        MessageConsumer consumer = session.createConsumer(queue);
        consumer.setMessageListener(new Listener());

        connection.start();
    }
}
